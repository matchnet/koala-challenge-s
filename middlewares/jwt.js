const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    return function (req, res, next) {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }
            if (token) {
                try {
                    let decoded = jwt.verify(token, process.env.key);
                    req.decoded = decoded;
                    next();
                } catch (err) {
                    console.log(err);
                    res.status(403).send({
                        success: false,
                        error: err
                    })
                }
            } else {
                return res.status(403).json({
                    success: false,
                    error: 'Invalid Token'
                })
            }

        } catch (error) {
            console.log(error);
            return res.status(403).json({
                success: false,
                error: 'Invalid Token'
            })
        }

        console.log("jwt middleware");
    };

}();